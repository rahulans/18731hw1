

#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"
#include <click/args.hh>
#include <fstream>
#include <string>
#include <vector>
#include <arpa/inet.h>


CLICK_DECLS


StatefulFirewall::StatefulFirewall(){};
StatefulFirewall::~StatefulFirewall(){};

vector<int> get_flags(const Packet *p)
{
    const click_ip *iph = p->ip_header();
    if (iph->ip_p == 6)
    {
        const click_tcp *newp = p->tcp_header();
        vector<int> flags;
        flags.push_back((newp->th_flags & (TH_SYN)) ? 1 : 0);
        flags.push_back((newp->th_flags & (TH_ACK)) ? 1 : 0);
        flags.push_back((newp->th_flags & (TH_FIN)) ? 1 : 0);
        flags.push_back((newp->th_flags & (TH_RST)) ? 1 : 0);
        
        
        
        
        return flags;
    }
}


Connection createConnobject(const Packet *p)
{
    const click_ip *iph = p->ip_header();
    int test = iph->ip_p;
    String srcip = inet_ntoa(iph->ip_src);
    String dstip = inet_ntoa(iph->ip_dst);

    int sp = -1, dp = -1, flag = -1, protonum = -1;
    unsigned long seq = -1, ack = -1;
    
    int swap = 0;
    protonum = iph->ip_p;
    if (iph->ip_p == 6)
    {

        const click_tcp *newp = p->tcp_header();
        sp = ntohs(newp->th_sport);
        dp = ntohs(newp->th_dport);
        seq = ntohl(newp->th_seq);
        ack = ntohl(newp->th_ack);
        vector<int> flags = get_flags(p);

        
        
        
    }
    String temp;
    int tempport;
    if (srcip.compare(dstip) >= 0)
    {
        
        tempport = sp;
        sp = dp;
        dp = tempport;
        temp = srcip;
        srcip = dstip;
        dstip = temp;
        swap = 1;
    }
    Connection newconn = Connection(srcip, dstip, sp, dp, seq, ack, protonum, swap);
    return newconn;
}

bool StatefulFirewall::check_if_new_connection(const Packet *p)
{
    vector<int> flags = get_flags(p);
    if (flags[3])
        return 1;
    Connection newconn = createConnobject(p);
    
    
    std::map<Connection, int>::iterator it = Connections.begin();
    int counter = 0;
    while (it != Connections.end())
    {
        const Connection current_conn = it->first;
        counter++;

        if (newconn.exact(current_conn))
        {
            

            

            return 0;
        }

        it++;
    }
    return 1;
};
int Policy::getAction()
{
    return action;
}

void StatefulFirewall::add_connection(Connection &c, int action)
{
    Connections.insert(std::pair<Connection, int>(c, action));

    
}

vector<int> check_policy(Connection current_conn, std::vector<Policy> list_of_policies)
{

    

    int action = -1;
    int status = 0;
    for (int i = 0; i < list_of_policies.size(); i++)
    {
        const Connection current_policy = list_of_policies[i].getConnection();
        if (current_conn.operator==(current_policy))
        {
            action = list_of_policies[i].getAction();

            
            status = 1;
            break;
        }
    }
    vector<int> result;
    result.push_back(status);
    result.push_back(action);
    return result;
}

void print1(std::map<Connection, int, cmp_connection> Connections)
{
    
    int counter = 0;
    for (std::map<Connection, int>::iterator it = Connections.begin(); it != Connections.end(); ++it)
    {
        
        it->first.print();
        counter++;
        
    }
    
    
    
    
    

    
    
}

int StatefulFirewall::filter_packet(const Packet *p)
{
    Connection newconn = createConnobject(p);

    
    vector<int> flags = get_flags(p);
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    static const int arr[] = {
        1,
        0,
        0,
        0,
    };
    vector<int> tSin(arr, arr + sizeof(arr) / sizeof(arr[0]));
    vector<int> policy_match = check_policy(newconn, list_of_policies);
    int found_inpolicy = policy_match[0];
    int action_inpolicy = policy_match[1];
    
    
    int ff = check_if_new_connection(p);
    int doaction = 0;
    if (flags == tSin)
    {
        int doaction = 0;
        if (found_inpolicy == 0)
            doaction = DEFAULTACTION;
        else
            doaction = action_inpolicy;

        if (ff == 0)
        {
            doaction = 0;
            std::map<Connection, int>::iterator it = Connections.begin();
            while (it != Connections.end())
            {
                const Connection current_conn = it->first;

                if (newconn.exact(current_conn))
                {
                    
                    Connections.erase(it);
                    
                    break;
                }
                it++;
            }
        }
        StatefulFirewall::add_connection(newconn, doaction);
        return doaction;
    }

    static const int arr1[] = {
        1,
        1,
        0,
        0,
    };
    vector<int> tSinAck(arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]));
    

    if (flags == tSinAck)
    {
        
        
        
        std::map<Connection, int>::iterator it = Connections.begin();
        Connection current_conn;
        int found = 0;
        while (it != Connections.end())
        {
            current_conn = it->first;
            if (newconn.operator==(current_conn))
            {
                
                
                
                int current_action = it->second;
                Connections.erase(it);

                

                if ((current_conn.is_forward() != newconn.is_forward()) && (newconn.get_destseq() == (current_conn.get_sourceseq() + 1)) && (current_conn.get_handshake_stat() == SYN))
                {
                    
                    
                    current_conn.update_handshake_stat(SYNACK);
                    current_conn.set_sourceseq(newconn.get_sourceseq());
                    current_conn.set_destseq(newconn.get_destseq());
                    
                    StatefulFirewall::add_connection(current_conn, current_action);
                    
                    return current_action;
                }
                else
                {
                    current_conn.update_handshake_stat(SYNACK);
                    current_conn.set_sourceseq(newconn.get_sourceseq());
                    current_conn.set_destseq(newconn.get_destseq());
                    
                    current_action = 0;
                    
                    

                    StatefulFirewall::add_connection(current_conn, 0);
                    return current_action;
                }

                
                
            }
            it++;
        }
    }
    static const int arr2[] = {0, 1, 0, 0};
    vector<int> tAck(arr2, arr2 + sizeof(arr2) / sizeof(arr2[0]));
    if (flags == tAck)
    {

        std::map<Connection, int>::iterator it = Connections.begin();
        Connection current_conn;
        int found = 0;
        while (it != Connections.end())
        {
            current_conn = it->first;
            if (newconn.operator==(current_conn))
            {
                if (current_conn.get_handshake_stat() == HS_DONE)
                    break;
                
                
                
                
                
                int current_action = it->second;
                Connections.erase(it);
                if (current_conn.get_handshake_stat() == HS_DONE)
                    break;
                

                if ((current_conn.is_forward() == newconn.is_forward()) && (newconn.get_destseq() == (current_conn.get_sourceseq() + 1)) && (newconn.get_sourceseq() == current_conn.get_destseq()) && (current_conn.get_handshake_stat() == SYNACK))
                {
                    
                    
                    current_conn.update_handshake_stat(HS_DONE);
                    current_conn.set_sourceseq(newconn.get_sourceseq());
                    current_conn.set_destseq(newconn.get_destseq());
                    
                    StatefulFirewall::add_connection(current_conn, current_action);
                    
                    return current_action;
                }
                else
                {
                    current_conn.update_handshake_stat(HS_DONE);
                    current_conn.set_sourceseq(newconn.get_sourceseq());
                    current_conn.set_destseq(newconn.get_destseq());
                    
                    current_action = 0;
                    
                    

                    StatefulFirewall::add_connection(current_conn, 0);
                    return current_action;
                }
            }
            it++;
        }
    }
    std::map<Connection, int>::iterator it1 = Connections.begin();
    while (it1 != Connections.end())
    {
        const Connection current_conn = it1->first;

        if (newconn.operator==(current_conn))
        {
            
            
            
            if (flags[3])
            {
                Connections.erase(it1);
                
            }
            return it1->second;
        }

        it1++;
    }
    if (found_inpolicy)
    {

        return action_inpolicy;
    }

    return DEFAULTACTION;
}
void StatefulFirewall::push(int, Packet *p)
{
    int res = filter_packet(p);

    output(res).push(p);
};
int StatefulFirewall::read_policy_config(String fileName)
{
    ifstream infile(fileName.c_str());

std:
    string sourceip, destip, destport, sourceport, proto, action;
    

    while (infile >> sourceip >> sourceport >> destip >> destport >> proto >> action)
    {
        
        
        
        

        
        

        

        

        
        String a = sourceip.c_str();
        String b = destip.c_str();

        int c = atoi(sourceport.c_str());

        
        int d = atoi(destport.c_str());
        int e = atoi(proto.c_str());
        int f = atoi(action.c_str());
        
        
        
        Policy pol = Policy(a, b, c, d, e, f);
        list_of_policies.push_back(pol);
        
    }

    return 1;
}

int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
    String policy_file;
    int def = -1;
    

    if (Args(conf, this, errh).read_p("POLICYFILE", policy_file).read_p("DEFAULT", def).complete() < 0)
    {
        
        
        
        return -1;
    }

    

    
    
    Vector<String> vecOfStr;
    DEFAULTACTION = def;

    int result = read_policy_config(policy_file);
    

    return 0;
}
Connection Policy::getConnection()
{
    
    String temp;
    int swap = 0;
    if (sourceip.compare(destip) >= 0)
    {
        

        temp = sourceip;
        sourceip = destip;
        destip = temp;
        swap = 1;
    }
    Connection newconn = Connection(sourceip, destip, sourceport, destport, 0, 0, proto, swap);
    
    return newconn;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
